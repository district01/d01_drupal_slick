# d01_drupal_slick #

DO1 implementation for slick (http://kenwheeler.github.io/slick/)
Provide a render element per component of the slick slider (Slider, Slide).

# Usage #

Create a slick slider render array. 

```

$thumbnails = [];
$slides = [];
foreach ($slides as $key => $slide) {

  $slides[$key] = [
    '#type' => 'd01_drupal_slick_slide',
    '#slide' => 'MY_SLIDE', // Can be a string or render array.
    '#attributes' => [
      'class' => [
        'm-carousel-slide',
      ],
    ],
  ];
  
  $thumbnails[$key] = [
    '#type' => 'd01_drupal_slick_slide',
    '#slide' => 'MY_SLIDE', // Can be a string or render array.
    '#attributes' => [
      'class' => [
        'm-carousel-thumb',
      ],
    ],
  ];
}

$variables['slick'] = [
  '#slick_id' => 'MY_SLICK_ID',
  '#slick_type' => 'MY_SLICK_TYPE', // For theming suggestions.
  '#attributes' => [
    'class' => [
      'm-carousel',
    ],
  ],
  '#type' => 'd01_drupal_slick',
  '#slides' => $slides,
  '#js_settings' => [
    'asNavFor' => '#' . MY_THUMBNAIL_ID,
    'initialSlide' => 0,
    'slidesToShow' => 1,
    'slidesToScroll' => 1,
    'fade' => TRUE,
    'dots' => FALSE,
    'infinite' => TRUE,
    'arrows' => TRUE,
    'prevArrow' => '<div class="m-carousel__prev a-button a-button--secondary a-button--sm a-button--round has-icon"><span class="icon-arrow-left"></span></div>',
    'nextArrow' => '<div class="m-carousel__next a-button a-button--secondary a-button--sm a-button--round has-icon"><span class="icon-arrow-right"></span></div>',
    'centerMode' => TRUE,
    'speed' => 250,
    'adaptiveHeight' => TRUE,
  ],
];

$variables['thumbnails'] = [
    '#slick_id' => 'MY_SLICK_ID',
    '#slick_type' => 'MY_SLICK_TYPE', // For theming suggestions.
    '#attributes' => [
      'class' => [
        'm-carousel-navigation',
      ],
    ],
    '#type' => 'd01_drupal_slick',
    '#slides' => $thumbnails,
    '#js_settings' => [
      'arrows' => FALSE,
      'dots' => FALSE,
      'centerMode' => TRUE,
      'centerPadding' => '0px',
      'focusOnSelect' => TRUE,
      'infinite' => TRUE,
      'swipe' => FALSE,
      'slidesToScroll' => 1,
      'slidesToShow' => 3,
      'speed' => 500,
      'asNavFor' => '#' . MY_SLICK_ID,
    ],
  ];


```

Accessing the lat/lon:

```

$address = $entity_helper->setType('general')->getValue($entity, 'field_address');
$lat = $address ? $address->getLatitude() : FALSE;
$lon = $address ? $address->getLongitude() : FALSE;

```


# Release notes #

`1.0`
* Initial setup of module.

`2.0`
* Make inclusion of the default slick theme css optional.

`3.0`
* Fix cache issue for slide element.

`4.0.0`
* Rework initialisation function, so initialisation is done in behavior.

`4.0.1`
* Fix typo in libraries.yml file.

`4.0.2`
* Fix add jQuery once as dependency of library.

`4.0.3`
* Fix Drupal 9 compatibility.