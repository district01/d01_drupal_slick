<?php

namespace Drupal\d01_drupal_slick\Element;

use Drupal\Core\Render\Element\RenderElement;
use Drupal\Core\Render\Element;

/**
 * An slick render element.
 *
 * @RenderElement("d01_drupal_slick")
 */
class D01DrupalSlick extends RenderElement {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $class = get_class($this);
    return [
      '#theme' => 'd01_drupal_slick',
      '#slick_id' => FALSE,
      '#slick_type' => 'slick',
      '#include_slick_css' => TRUE,
      '#js_settings' => [],
      '#attributes' => [],
      '#slides' => [],
      '#pre_render' => [
        [$class, 'preRenderElement'],
      ],
      '#attached' => [
        'library' => [
          'd01_drupal_slick/slick',
        ],
        'drupalSettings' => [
          'd01_drupal_slick' => [],
        ],
      ],
    ];
  }

  /**
   * Prerender function for Slick element.
   */
  public static function preRenderElement($element) {

    // Make sure we only #pre_render item once.
    if (!empty($element['#pre_rendered'])) {
      return $element;
    }

    // Include the original slick theme css.
    if ($element['#include_slick_css']) {
      $element['#attached']['library'][] = 'd01_drupal_slick/slick_css';
    }

    // We need a #slick_id for theme suggestions and js-settings.
    // This way we can have more than one element on the page with different
    // settings.
    if (!$element['#slick_id']) {
      $element = [
        '#markup' => t('The d01_drupal_slick element requires an id to work.'),
      ];
      return $element;
    }

    // We need to make sure all children added in #slides
    // are d01_drupal_slick_slide items. So we remove all
    // other items.
    $count = 0;
    if (!empty($element['#slides'])) {
      foreach ($element['#slides'] as $key => $child) {
        if (!D01DrupalSlickSlide::isSlickSlideItem($child)) {
          unset($element['#slides'][$key]);
        }
        else {
          // Add the parent slick_id and the position of the slide to
          // the children. This way we can provide more specific
          // theme suggestions.
          //
          $element['#slides'][$key]['#position'] = $count;
          $element['#slides'][$key]['#parent_slick_id'] = isset($element['#slick_id']) ? $element['#slick_id'] : FALSE;
          $element['#slides'][$key]['#parent_slick_type'] = $element['#slick_type'];
          $count++;
        }
      }
    }

    // Convert #slides to a renderable element.
    $element['slides'] = !empty($element['#slides']) ? $element['#slides'] : [];

    // Since we can't have a slider without slides we
    // return the element which will be an empty array at this point.
    if (!empty($element['slides'])) {

      // Get the #slick_id and the #js_settings.
      $js_id = $element['#slick_id'];
      $js_settings = is_array($element['#js_settings']) ? $element['#js_settings'] : [];

      // Set the #slick_id as html Id.
      $element['#attributes']['id'] = $js_id;

      // Add required JS classes.
      $element['#attributes']['class'][] = 'js-d01-drupal-slick';

      // Pass the settings to js keyed by the #slick_id.
      $element['#attached']['drupalSettings']['d01_drupal_slick'][$js_id] = $js_settings;
    }

    // Mark item as prerendered.
    $element['#pre_rendered'] = TRUE;
    return $element;
  }

}
