<?php

namespace Drupal\d01_drupal_slick\Element;

use Drupal\Core\Render\Element\RenderElement;
use Drupal\Core\Render\Element;

/**
 * An slick slide render element.
 *
 * @RenderElement("d01_drupal_slick_slide")
 */
class D01DrupalSlickSlide extends RenderElement {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $class = get_class($this);
    return [
      '#theme' => 'd01_drupal_slick_slide',
      '#attributes' => [],
      '#slide' => [],
      '#position' => FALSE,
      '#parent_slick_id' => FALSE,
      '#parent_slick_type' => 'slick',
      '#pre_render' => [
        [$class, 'preRenderElement'],
      ],
    ];
  }

  /**
   * Make sure we only have accordion items as children.
   */
  public static function preRenderElement($element) {

    // Make sure we only #pre_render item once.
    if (!empty($element['#pre_rendered'])) {
      return $element;
    }

    // We need a render array for our slick slide element
    // but we want to give people full freedom to pass whatever
    // they want to the slide element.So we need to check if we recieve
    // a render array and else we need to convert it to a render array.
    if (!D01DrupalSlickSlide::isRenderableArray($element['#slide'])) {
      // Convert string to render array.
      $element['#slide'] = ['#markup' => $element['#slide']];
    }

    // Add JS classes as attributes.
    $element['#attributes']['class'][] = 'js-d01-drupal-slick-slide';

    // Convert #slide to a renderable element.
    $element['slide'] = $element['#slide'];

    // Mark item as prerendered.
    $element['#pre_rendered'] = TRUE;
    return $element;
  }

  /**
   * Is renderable array.
   *
   * Check if the passed item is a render array.
   *
   * @param mixed $element
   *   A item to check.
   *
   * @return bool
   *   A boolean indicating if item contains #theme, #type or #markup.
   */
  private static function isRenderableArray($element) {

    // Check if we got an array.
    if (!is_array($element)) {
      return FALSE;
    }

    // Check for #theme or #type key.
    if (isset($element['#theme']) || isset($element['#type']) || isset($element['#markup'])) {
      return TRUE;
    }
    else {
      return FALSE;
    }
  }

  /**
   * Is slick slide item.
   *
   * Check if the passed item is a render
   * array that is an implementation of
   * d01_drupal_slick_slide.
   *
   * @param mixed $element
   *   A item to check.
   *
   * @return bool
   *   A boolean indicating if item is of type d01_drupal_slick_slide.
   */
  public static function isSlickSlideItem($element) {

    // Check if array.
    if (!is_array($element)) {
      return FALSE;
    }

    // Check if it's a render element.
    if (!isset($element['#type'])) {
      return FALSE;
    }

    // Check if it's a d01_drupal_slick_slide item.
    if ($element['#type'] !== 'd01_drupal_slick_slide') {
      return FALSE;
    }

    return TRUE;
  }
}
