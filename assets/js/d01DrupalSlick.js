(function ($) {

  'use strict';

  /**
   * jQuery object
   * @external jQuery
   * @see {@link http://api.jquery.com/jQuery/}
   */

  /**
   * D01DrupalSwipers
   *
   * @param {jQuery} el
   *    css class of the slick wrapper.
   * @param {string} slideClass
   *    css class of the slick items.
   * @param {object} settings
   *    an object containing all settings.
   * @constructor
   */
  var D01DrupalSlick = function (el, slideClass, settings) {
    var that = this;
    that.el = el;
    that.slideClass = slideClass;
    that.settings = settings || {};
  };

  /**
   * initializeSlicks().
   */
  D01DrupalSlick.prototype.init = function () {
    var that = this;

    var slickSettings = that.settings || {};
    slickSettings.slide = that.slideClass;
    that.el.slick(slickSettings);
  };

  /**
   * @type {D01DrupalSlick}
   */
  window.D01DrupalSlick = D01DrupalSlick;

})(jQuery);
