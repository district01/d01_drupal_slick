/**
 * d01DrupalSlick.
 */
(function ($, Drupal) {

  'use strict';

  /**
   * D01 Drupal Slick behavior.
   * @type {{attach: Drupal.behaviors.d01_drupal_slick.attach}}
   */
  Drupal.behaviors.d01_drupal_slick = {
    attach: function (context, drupalSettings) {
      var componentSettings = drupalSettings.d01_drupal_slick || {};
      var wrapperClass = '.js-d01-drupal-slick';
      $(wrapperClass, context).once('initializeSlicks').each(function(i, obj) {
        var el = $(this);
        var elId = el.attr('id') ? el.attr('id') : false;
        if (elId) {
          var options = componentSettings[elId] || {};
          new D01DrupalSlick(
            el,
            '.js-d01-drupal-slick-slide',
            options
          ).init();
        }
      });

    }
  };
})(jQuery, Drupal);
